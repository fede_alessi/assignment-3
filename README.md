# Assignment 3

This project stands for the third assignment for the course "Processo e sviluppo
del software" at Milano's Bicocca University.<br>
Link to the repo: https://gitlab.com/federicoalessi3e/assignment-3

## Description ##

The goal of this project is to build an application to ease the handling gyms' data
in cities. Front-end part won't be included but it could be implemented in future.

This project will concern four Entities that will follow the ER diagram in the 
`Model.pdf` file, included in this repo.

These entities are:
* City
* Gym
* PersonalTrainer
* User

Relationships are defined among this entities: these can be seen in the model defined
above and they're descripted in detail in the next sections.

## Entities ##
* City
    * CAP <b>(PK)</b>
    * Abitanti (int)
    * Regione  (varchar)
    * Nome (varchar)

* Gym
    * ID <b>(PK)</b>
    * open24H (bool)
    * City <b>(FK di City)</b>

* User
    * Cod_Fiscale <b>(PK)</b>
    * Nome (varchar)
    * Cognome (varchar)
    * Età (int)
    * PalestraID <b>(FK di Gym)</b>
    * PersonalTrainerID <b>(FK di Personal Trainer)</b>
    * ID_Invito <b>(FK della self-relationship)</b>

* Personal Trainer
    * ID <b>(PK)</b>
    * Cognome
    * Sesso
    * Età
    * PalestraID <b>(FK di Gym)</b>


## Relationships' description ##

* Between 'City' and 'Gym': one city could have from 0 to
n gyms and , of course, a gym can be only in one city. Coding it, this is been inteded as a `ManyToOne`
relationship in the Gym entity.

* Between 'Gym' and 'Users': realistically a user is subscrived only to one gym and and a gym
has many users. Another `ManyToOne` relationship in the User entity.

* Finally the self-relation: A user can invite another person to join the gym, in order to 
obtain a discount from his next subscription. Even if in some gyms users can invite an unlimited
number of user, I decided to model my entity on my own gym: a user can invite only a person in order
to gain a free-month-promotion. This relationship is been coded as a `OneToOne` relationship.

Even if the project would have to be designed including just three relationship, I add
other ones to complete the logical links between the entities.


* Between 'PersonalTrainer' and 'Users': a user can be trained from one personal trainer
and a personal trainer can train more than one user (or none). The relationship is `ManyToOne` in the User
entity.
* Between 'PersonalTrainer' and 'Gym': one personal trainer can work in one gym and a gym 
could have more then one personal trainer. Even in this case the relationship is `ManyToOne`
in the User entity.


## Relationships' consequences ##
My choices in mapping the relationships in the entites had some consequences on some methods:
* if a personal trainer is deleted from the db, his ID will be set to NULL in the 
all the users that are trained by him. 
* If a gym is deleted from the db all of its users and the trainers that work there will be
deleted to in the db. This can be explained because if a gym is deleted (let's say for failure,
demolished etc etc) all the entites related to it won't be able to attend it anymore, so it makes
sense to deleted all the related entites too. This is a bit different from the previous point
of this list: if a trainer is sacked or change gym the users trained by him won't be able to
be trained from him anymore, but still they will be able to attend the gym and change trainer. So it's
ok to keep users in the db even if their personal trainer is deleted.
* If a city is deleted from the db all the gyms in that city will be deleted too (and so the personal trainer and user of 
those gyms) for the same reason of the previous point.

## Project setup ##
* Download this repository.
* Import this project as a Maven project using IntelliJ (the same IDE used to develop the project).
* To configure a right connection to your database go in the `persistence.xml` file in the META-INF folder and
change the username and password options by inserting your own ones by substituting `YOURUSERNAME` and `YOURPASSWORD`
in the right tag. 
If the connection is successfull a schema called `assignment_3` will be created automatically if
not present yet.

Once the project is open in your IDE go in the test folder in whatever test file:
be sure that all the classes in there are imported in the right way, if not use the IDE suggestions to fix it up.
Another important thing to check up before running the tests is to check if everything's ok with
daos. In fact, it may happens that the project is not imported correctly, and for this reason the dependences between
test folder and main folder are lost. To recover them just follow the IDE suggestions that will fix it up automatically.

## Responsibility assigned ##
All the source code can be found in the <code>src/main</code>.
Starting from the Entities, already described in a section above, they can be found in the 'model'
package.

* To perform CRUD methods I created DAOs on these entities: first of all a generic abstract class
called Dao creates the configuration with the entity manager and let it available to
subclasses with a method called 'getEntityManager'. Furthermore this class has two more methods: one to begin a transaction
and another one to commit it. Finally this class has some templated methods that will be overriden in
the subclasses.
    + all() to have a list of all the entities of a certain type from the db
    + get() to have a specific entity from the db
    + create() to create an entity 
    + update() to update an entity 
    + delete() to delete an entity
These methods are implemented for each Entity in the model folder. 
The dao classes implement these methods but in tests won't be directly used, because
they only create the logic of the operations: the classes that will use this logic will
be described in the next paragraph.

* In the <code>services</code> paragraph I created four classes that incapsulate the dao
classes and create a further level of separation between usage and logic. This classes take
the correspondend dao class and create an instance of it. Thanks to this the services can
re-implement the same five methods of dao classes adding the beginning of the transaction and its 
commit.
<b>Explanation of this implementation</b>
Moreover is usefull to create a service layer over the dao one because, generally, in the dao
the connection with the db is set, and so using a further layer on it  provide level that has no relation to the DB, 
so it's more difficult to gain access to the DB from the client except through the service.
Finally, the dao layer should be as lite as possible, because in more structured projects a single dao can 
provide different connections with differend dbs, and for this reason all the logic of the methods should be provided
from a different layer, that is the service one.

## Test ##
To test the correctivness of all the CRUD methods implemented for every entity I used unit tests.
In this project there are four files, in the `test` folder, each one tests a single entity and its method.
Some entites have been tested a bit longer than others: for example <b>User</b> entity has more tests because
I wanted to make sure that if a gym was deleted, all the users of that gym would have been deleted too, and so for
the field 'PersonalTrainerID' set to null if a trainer was deleted from the db. 

To refactor code (tons of duplicate code) I created a method called 'createNAMEOFTHEENTITY' that takes all the arguments
of that entity and persist it in the db: I could use this method to avoid code duplication and do, in a single step creation
of the entity by its constructor and persist it on the db.

Moreover I used a class called DBDrop that has only one method to drop every data in the db before all the test methods are run
(with the @Before annotation). So everytime a test case is run the first things that happen are: creation of the necessary services
to test an entity and drop of the db thanks to DBDrop class.

27 tests have been created to test the four entites in this project.

To run them just run `mvn test` in your terminal and all the test classes will automatically run. 
I also included a sample file in `src/boot`called Main, that loads some entities on the db and performs some CRUD methods on them.

