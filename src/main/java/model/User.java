package model;


import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name=User.TABLE_NAME)
public class User {

    public static final String TABLE_NAME = "User";

    @Id
    @Column(name = "COD_FISCALE")
    @GeneratedValue( strategy= GenerationType.AUTO )
    private int codiceFiscale;

    @Column(name = "Name")
    private String nome;

    @Column(name = "Lastname")
    private String cognome;

    @Column(name = "Age")
    private int età;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "PersonalTrainer_ID")
    private PersonalTrainer personalTrainerId;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinColumn(name = "Palestra_ID")
    private Gym palestraId;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_Invito")
    private User idInvito;

    public User(){}

    public User(String nome, String cognome, int età, Gym palestra){
        this.setNome(nome);
        this.setCognome(cognome);
        this.setEtà(età);
        this.setPalestraId(palestra);

    }

    public int getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(int codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public int getEtà() {
        return età;
    }

    public void setEtà(int età) {
        this.età = età;
    }

    public PersonalTrainer getPersonalTrainerId() {
        return personalTrainerId;
    }

    public void setPersonalTrainerId(PersonalTrainer personalTrainerId) {
        this.personalTrainerId = personalTrainerId;
    }

    public Gym getPalestraId() {
        return palestraId;
    }

    public void setPalestraId(Gym palestraId) {
        this.palestraId = palestraId;
    }

    public User getIdInvito() {
        return idInvito;
    }

    public void setIdInvito(User idInvito) {
        this.idInvito = idInvito;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.codiceFiscale);
    }

    @Override
    public String toString() {
        return "User{" +
                "codiceFiscale=" + codiceFiscale +
                ", nome='" + nome + '\'' +
                ", cognome='" + cognome + '\'' +
                '}';
    }
}
