package model;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name=City.TABLE_NAME)
public class City {

    public static final String TABLE_NAME = "City";

    @Id
    @Column(name = "CAP")
    private int cap;

    @Column(name = "Name")
    private String nome;

    @Column(name = "Region")
    private String regione;

    @Column(name = "Inhabitants")
    private int abitanti;


    public City(){}

    public City(int cap, int abitanti, String regione, String nome){
        this.setCap(cap);
        this.setNome(nome);
        this.setRegione(regione);
        this.setAbitanti(abitanti);
    }

    public int getCap() {
        return cap;
    }

    public void setCap(int cap) {
        this.cap = cap;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRegione() {
        return regione;
    }

    public void setRegione(String regione) {
        this.regione = regione;
    }

    public Integer getAbitanti() {
        return abitanti;
    }

    public void setAbitanti(Integer abitanti) {
        this.abitanti = abitanti;
    }

    @Override
    public String toString() {
        return "City{" +
                "cap=" + cap +
                ", nome='" + nome + '\'' +
                ", regione='" + regione + '\'' +
                ", abitanti=" + abitanti +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCap(), getNome(), getRegione(), getAbitanti());
    }
}
