package services;

import daos.CityDao;
import model.City;
import model.Gym;

import java.util.List;

public class CityService {
    private CityDao citydao;

    public CityService(){
        this.citydao = new CityDao();
    }

    public City get(Integer id){
        citydao.beginTransaction();
        City city = citydao.get(id);
        citydao.commitTransaction();

        return city;
    }

    public List<City> all(){
        citydao.beginTransaction();
        List<City> cities = citydao.all();
        citydao.commitTransaction();

        return cities;
    }

    public City create(City city){
        citydao.beginTransaction();
        city = citydao.create(city);
        citydao.commitTransaction();

        return city;
    }

    public void delete(City city){
        GymService gymService= new GymService();
        List<Gym> gyms = gymService.all();

        for(Gym gym: gyms){
            if(gym.getCity() != null && (gym.getCity().getCap() == city.getCap())){
                gymService.delete(gym);
            }
        }
        citydao.beginTransaction();
        citydao.delete(city);
        citydao.commitTransaction();
    }

    public City update(City city){
        citydao.beginTransaction();
        city = citydao.update(city);
        citydao.commitTransaction();

        return city;
    }

}
