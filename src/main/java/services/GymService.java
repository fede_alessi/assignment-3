package services;

import daos.GymDao;
import model.Gym;
import model.User;
import model.PersonalTrainer;

import java.util.List;

public class GymService {
    private GymDao gymdao;

    public GymService(){
        this.gymdao = new GymDao();
    }

    public Gym get(Integer id){
        gymdao.beginTransaction();
        Gym gym = gymdao.get(id);
        gymdao.commitTransaction();

        return gym;
    }

    public List<Gym> all(){
        gymdao.beginTransaction();
        List<Gym> gyms = gymdao.all();
        gymdao.commitTransaction();

        return gyms;
    }

    public Gym create(Gym gym){
        gymdao.beginTransaction();
        gym = gymdao.create(gym);
        gymdao.commitTransaction();

        return gym;
    }

    public void delete(Gym gym){


        UserService userservice= new UserService();
        List<User> users = userservice.all();

        PersonalTrainerService trainerservice = new PersonalTrainerService();
        List<PersonalTrainer> trainers = trainerservice.all();

        for(User user: users){
            if(user.getPalestraId() != null && user.getPalestraId().getId() == gym.getId()) {
                userservice.delete(user);
            }
        }

        for(PersonalTrainer trainer: trainers){
            if(trainer.getPalestraId() != null && trainer.getPalestraId().getId() == gym.getId()) {
                trainerservice.delete(trainer);
            }
        }

        gymdao.beginTransaction();
        gymdao.delete(gym);
        gymdao.commitTransaction();
    }

    public Gym update(Gym gym){
        gymdao.beginTransaction();
        gym = gymdao.update(gym);
        gymdao.commitTransaction();

        return gym;
    }
}
