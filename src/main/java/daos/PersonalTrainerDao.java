package daos;

import model.PersonalTrainer;
import java.util.List;

public class PersonalTrainerDao extends Dao<PersonalTrainer, Integer>{
    @Override
    public List<PersonalTrainer> all() {
        return getEntityManager().createQuery("select trainer from PersonalTrainer trainer").getResultList();
    }

    @Override
    public PersonalTrainer get(Integer id) {
        PersonalTrainer trainer = getEntityManager().find(PersonalTrainer.class, id);

        return trainer;
    }

    @Override
    public PersonalTrainer create(PersonalTrainer trainer) {
        getEntityManager().persist(trainer);

        return trainer;
    }


    @Override
    public void delete(PersonalTrainer trainer) {
        if(getEntityManager().find(PersonalTrainer.class, trainer.getId()) != null) {
            getEntityManager().remove(trainer);
        } else {
            System.out.println("Impossible to delete this trainer: it doesn't exist on the db");
        }

    }

    @Override
    public PersonalTrainer update(PersonalTrainer trainer) {
        PersonalTrainer p = getEntityManager().merge(trainer);

        return p;
    }

}
