package daos;

import model.City;
import java.util.List;

public class CityDao extends Dao<City, Integer>{
    @Override
    public List<City> all() {
        return getEntityManager().createQuery("select city from City city").getResultList();
    }

    @Override
    public City get(Integer id) {
        return getEntityManager().find(City.class, id);
    }

    @Override
    public City create(City city) {
        getEntityManager().persist(city);
        return city;
    }


    @Override
    public void delete(City city) {
        if(getEntityManager().find(City.class, city.getCap()) != null) {
            getEntityManager().remove(city);
        } else {
            System.out.println("Impossible to delete this city: it doesn't exist on the db");
        }

    }

    @Override
    public City update(City city) {
        return getEntityManager().merge(city);
    }

}

