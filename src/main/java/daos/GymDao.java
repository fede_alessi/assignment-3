package daos;

import model.Gym;
import java.util.List;

public class GymDao extends Dao<Gym, Integer>{
    @Override
    public List<Gym> all() {
        return getEntityManager().createQuery("select gym from Gym gym").getResultList();
    }

    @Override
    public Gym get(Integer id) {
        Gym gym = getEntityManager().find(Gym.class, id);
        return gym;
    }

    @Override
    public Gym create(Gym gym) {
        getEntityManager().persist(gym);
        return gym;
    }


    @Override
    public void delete(Gym gym) {
        if(getEntityManager().find(Gym.class, gym.getId()) != null) {
            getEntityManager().remove(gym);
        } else {
            System.out.println("Impossible to delete this gym: it doesn't exist on the db");
        }

    }

    @Override
    public Gym update(Gym gym) {
        getEntityManager().merge(gym);
        return gym;
    }

}
