package daos;

import model.User;
import java.util.List;

public class UtenteDao extends Dao<User, Integer>{
    @Override
    public List<User> all() {
        return getEntityManager().createQuery("select user from User user").getResultList();
    }

    @Override
    public User get(Integer id) {
        User user = getEntityManager().find(User.class, id);

        return user;
    }

    @Override
    public User create(User user) {
        getEntityManager().persist(user);

        return user;
    }


    @Override
    public void delete(User user) {
        if(getEntityManager().find(User.class, user.getCodiceFiscale()) != null) {
            getEntityManager().remove(user);
        } else {
            System.out.println("Impossible to delete this user: it doesn't exist on the db");
        }

    }

    @Override
    public User update(User user) {
        getEntityManager().merge(user);

        return user;
    }

}



