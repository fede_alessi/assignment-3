package boot;

import services.*;
import model.*;

import java.util.List;

public class Main {
    private static UserService userService = new UserService();
    private static GymService gymService = new GymService();
    private static PersonalTrainerService personalTrainerService = new PersonalTrainerService();
    private static CityService cityService = new CityService();

    public static void main(String args[]){
        //Cities
        City city = new City(20010, 15000, "Lombardia", "Canegrate");
        City city2 = new City(20020, 12300, "Lombardia", "San Vittore");

        //Gyms
        Gym gym = new Gym(true, city);
        Gym gym2 = new Gym(false, city);
        Gym gym3 = new Gym(true, city);
        Gym gym4 = new Gym(true, city2);
        Gym gym5 = new Gym(false, city2);


        //Trainers
        PersonalTrainer trainer = new PersonalTrainer("Polio", "Maschio", 32, gym);
        PersonalTrainer trainer2 = new PersonalTrainer("Nudo", "Maschio", 21, gym);
        PersonalTrainer trainer3 = new PersonalTrainer("Zini", "Femmina", 25, gym2);
        PersonalTrainer trainer4 = new PersonalTrainer("Cupa", "Maschio", 43, gym3);
        PersonalTrainer trainer5 = new PersonalTrainer("Cenu", "Femmina", 20, gym3);
        PersonalTrainer trainer6 = new PersonalTrainer("Foo", "Maschio", 51, gym4);
        PersonalTrainer trainer7 = new PersonalTrainer("Cotolo", "Femmina", 42, gym5);
        PersonalTrainer trainer8 = new PersonalTrainer("Caboto", "Maschio", 32, gym5);
        PersonalTrainer trainer9 = new PersonalTrainer("Skusku", "Femmina", 777, gym5);

        //Users
        User user = new User("Federico", "Alessi", 22, gym);
        User user2 = new User("Luca", "Pagani", 22, gym);
        User user3 = new User("Riccardo", "Dellavedova", 22, gym2);
        User user4 = new User("Davide", "Ponzoni", 22, gym3);
        User user5 = new User("Sasa", "Pone", 22, null);

        user.setPersonalTrainerId(trainer);
        user.setIdInvito(user2);
        user2.setPersonalTrainerId(trainer2);

        user3.setPersonalTrainerId(trainer3);
        user4.setPersonalTrainerId(trainer4);

        //Create
        city = cityService.create(city);
        city2 = cityService.create(city2);

        gym = gymService.create(gym);
        gym2 = gymService.create(gym2);
        gym3 = gymService.create(gym3);
        gym4 = gymService.create(gym4);
        gym5 = gymService.create(gym5);

        user = userService.create(user);
        user2 = userService.create(user2);
        user3 = userService.create(user3);
        user4 = userService.create(user4);
        user5 = userService.create(user5);


        trainer = personalTrainerService.create(trainer);
        trainer2 = personalTrainerService.create(trainer2);
        trainer3 = personalTrainerService.create(trainer3);
        trainer4 = personalTrainerService.create(trainer4);
        trainer5 = personalTrainerService.create(trainer5);
        trainer6 = personalTrainerService.create(trainer6);
        trainer7 = personalTrainerService.create(trainer7);
        trainer8 = personalTrainerService.create(trainer8);
        trainer9 = personalTrainerService.create(trainer9);

        //READ
        List<City> cities = cityService.all();

        System.out.println("-------Cities------");
        for(City c:cities){
            System.out.println(c.toString());
        }

        List<Gym> gyms = gymService.all();

        System.out.println("-------Gyms------");
        for(Gym g:gyms){
            System.out.println(g.toString());
        }

        List<User> users = userService.all();

        System.out.println("-------Users------");
        for(User u:users){
            System.out.println(u.toString());
        }

        List<PersonalTrainer> trainers = personalTrainerService.all();

        System.out.println("-------Trainers------");
        for(PersonalTrainer t:trainers){
            System.out.println(t.toString());
        }

        //UPDATE
        user.setEtà(23);
        user = userService.update(user);

        //DELETE

        userService.delete(user);
        userService.delete(user4);
        personalTrainerService.delete(trainer);
        gymService.delete(gym2);

    }
}
